package ru.myachin.access.local;

public class AccessControl {

    public int i4;
    protected int i3;
    /*package-private*/ int i2;
    private int i1;

    public static void main(String[] args) {
        AccessControl accessControl = new AccessControl();
        accessControl.i1 = 1;
        accessControl.i2 = 2;
        accessControl.i3 = 3;
        accessControl.i4 = 4;

        accessControl.privateMethod();
        accessControl.packagePrivateMethod();
        accessControl.protectedMethod();
        accessControl.publicMethod();
    }

    private void privateMethod() {
        System.out.println("privateMethod");
    }

    /*package-private*/ void packagePrivateMethod() {
        System.out.println("packagePrivateMethod");
    }

    protected void protectedMethod() {
        System.out.println("protectedMethod");
    }

    public void publicMethod() {
        System.out.println("publicMethod");
    }
}
