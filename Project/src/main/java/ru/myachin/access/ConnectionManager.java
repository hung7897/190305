package ru.myachin.access;

public class ConnectionManager {

    private static Connection[] connections = new Connection[]{
            new Connection(),
            new Connection(),
            new Connection()
    };

    private static int freeConnections = connections.length;

    public static Connection getConnection() {
        if (freeConnections != 0) {
            return connections[connections.length - freeConnections--];
        }

        return null;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println(getConnection());
        }
    }
}

class Connection {

    Connection() {

    }
}
