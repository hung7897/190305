package ru.myachin.access;

class WithProtected {

    protected int i;
}

class ProtectedManipulation {

    public static void main(String[] args) {
        System.out.println(new WithProtected().i);
    }
}