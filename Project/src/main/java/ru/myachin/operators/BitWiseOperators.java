package ru.myachin.operators;

public class BitWiseOperators {

    public static final int FIRST_INTEGER = 0x0f28fc36b;
    public static final int SECOND_INTEGER = 0xff89df90;

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(FIRST_INTEGER));
        System.out.println(Integer.toBinaryString(SECOND_INTEGER));
        System.out.println(Integer.toBinaryString(~FIRST_INTEGER));
        System.out.println(Integer.toBinaryString(~SECOND_INTEGER));
        System.out.println(Integer.toBinaryString(FIRST_INTEGER & SECOND_INTEGER));
        System.out.println(Integer.toBinaryString(FIRST_INTEGER | SECOND_INTEGER));
        System.out.println(Integer.toBinaryString(FIRST_INTEGER ^ SECOND_INTEGER));
    }
}