package ru.myachin.operators;

public class SignedRightShift {

    public static void main(String[] args) {
        int i = Integer.MIN_VALUE;
        for (int j = 0; j < 32; j++) {
            System.out.println(Integer.toBinaryString(i));
            i = rightShift(i);
        }
    }

    static int rightShift(int i) {
        return i >> 1;
    }
}