package ru.myachin.operators;

public class Dog2 {

    public static void main(String[] args) {
        Dog spot = new Dog("spot", "My name is spot");
        Dog anotherDog = new Dog("anotherDog", "I don't know my name");
        anotherDog = spot;

        System.out.println(spot == anotherDog);
        System.out.println(spot.equals(anotherDog));
    }
}