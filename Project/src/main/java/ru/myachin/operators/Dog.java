package ru.myachin.operators;

public class Dog {

    String name;
    String says;

    public Dog(String name, String says) {
        this.name = name;
        this.says = says;
    }

    public static void main(String[] args) {
        Dog spot = new Dog("spot", "My name is spot");
        Dog scruffy = new Dog("scruffy", "My name is scruffy");

        System.out.println(spot.name + ": " + spot.says);
        System.out.println(scruffy.name + ": " + scruffy.says);
    }
}