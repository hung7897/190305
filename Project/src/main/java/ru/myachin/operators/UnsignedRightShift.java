package ru.myachin.operators;

public class UnsignedRightShift {

    public static void main(String[] args) {
        int i = -1 << 1;
        for (int j = 0; j < 32; j++) {
            System.out.println(Integer.toBinaryString(i));
            i = rightShift(i);
        }
    }

    static int rightShift(int i) {
        return i >>> 1;
    }
}