package ru.myachin.operators;

public class BinaryChar {

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString('a'));
        System.out.println(Integer.toBinaryString('b'));
        System.out.println(Integer.toBinaryString('^'));
        System.out.println(Integer.toBinaryString('&'));
        System.out.println(Integer.toBinaryString('1'));
        System.out.println(Integer.toBinaryString('0'));
        System.out.println(Integer.toBinaryString('!'));
    }
}