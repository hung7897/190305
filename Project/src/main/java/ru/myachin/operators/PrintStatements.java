package ru.myachin.operators;

public class PrintStatements {

    public static void main(String[] args) {
        System.out.println("Длинная команда печати.");
        print("Короткая команда печати.");
    }

    static void print(String s) {
        System.out.println(s);
    }
}